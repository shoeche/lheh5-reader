#include "hdf5_event_reader.h"

using namespace HighFive;

namespace LHEH5 {

  class LHEOutput {

    File *p_file;
    std::map<std::string,DataSet> m_dss;
    std::vector<std::vector<double> > m_ecache, m_pcache;
    std::vector<std::vector<double> > m_necache, m_npcache;
    double m_xs, m_xserr, m_max, m_trials;
    std::string m_basename;
    int m_hasnlo, m_ncache, m_events, m_offset, m_nmax;
    size_t m_nweights, m_neprops, m_npprops, m_clabel;
    size_t m_nneprops, m_nnpprops;

  public:

    LHEOutput(const std::string &outfile,const int hasnlo):
      m_xs(1.0), m_xserr(1.0), m_max(1.0), m_trials(0.0),
      m_hasnlo(hasnlo), m_events(0), m_offset(0), m_nmax(0)
    {
      HighFive::FileAccessProps fprops;
      fprops.add(HighFive::MPIOFileAccess(MPI_COMM_WORLD,MPI_INFO_NULL));
      fprops.add(HighFive::MPIOCollectiveMetadata());
      p_file = new File
	(outfile,File::ReadWrite|File::Create|File::Truncate,fprops);
    }

    ~LHEOutput()
    {
      m_dss.clear();
      delete p_file;
    }

    void Header(const std::vector<double> &idata,
		const std::vector<std::vector<double> > &pdata)
    {
      auto xfer_props = DataTransferProps{};
      xfer_props.add(UseCollectiveIO{});
      std::vector<int> versionno = {2, 0, 0};
      m_dss["version"]=p_file->createDataSet<int>
	("version",DataSpace::From(versionno));
      m_dss["version"].write(versionno, xfer_props);
      std::vector<std::string> inames;
      inames.push_back("beamA");
      inames.push_back("beamB");
      inames.push_back("energyA");
      inames.push_back("energyB");
      inames.push_back("PDFgroupA");
      inames.push_back("PDFgroupB");
      inames.push_back("PDFsetA");
      inames.push_back("PDFsetB");
      inames.push_back("weightingStrategy");
      inames.push_back("numProcesses");
      DataSetCreateProps props;
      m_dss["init"]=p_file->createDataSet<double>
	("init",DataSpace::From(idata));
      m_dss["init"].write(idata, xfer_props);
      m_dss["init"].createAttribute<std::string>
	("properties",DataSpace::From(inames)).write(inames);
      std::vector<std::string> pnames(6);
      pnames[0]="procId";
      pnames[1]="npLO";
      pnames[2]="npNLO";
      pnames[3]="xSection";
      pnames[4]="error";
      pnames[5]="unitWeight";
      m_dss["procInfo"]=p_file->createDataSet<double>
	("procInfo",DataSpace::From(pdata));
      m_dss["procInfo"].write(pdata, xfer_props);
      m_dss["procInfo"].createAttribute<std::string>
	("properties",DataSpace::From(pnames)).write(pnames);
    }

    void Initialize(const size_t &nup,const size_t &nevts)
    {
      size_t size(MPI::COMM_WORLD.Get_size());
      std::vector<size_t> min(1,size);
      min.front()*=m_ncache=nevts;
      std::vector<size_t> max(min);
      DataSetCreateProps props;
      std::vector<std::string> wnames(1,"NOMINAL");
      m_nweights=wnames.size();
      std::vector<std::string> enames((m_neprops=9)+m_nweights);
      enames[0]="pid";
      enames[1]="nparticles";
      enames[2]="start";
      enames[3]="trials";
      enames[4]="scale";
      enames[5]="fscale";
      enames[6]="rscale";
      enames[7]="aqed";
      enames[8]="aqcd";
      for (size_t i(0);i<wnames.size();++i)
	enames[m_neprops+i]=wnames[i];
      min.push_back(enames.size());
      max.push_back(enames.size());
      props=DataSetCreateProps();
      m_dss["events"]=p_file->createDataSet<double>
	("events",DataSpace(min,max),props);
      m_ecache.reserve(m_ncache);
      for (size_t i(0);i<m_ecache.size();++i)
	m_ecache[i].reserve(m_neprops+m_nweights);
      m_dss["events"].createAttribute<std::string>
	("events",DataSpace::From(enames)).write(enames);
      if (m_hasnlo) {
	std::vector<std::string> nenames(m_nneprops=9);
	nenames[0]="ijt";
	nenames[1]="kt";
	nenames[2]="i";
	nenames[3]="j";
	nenames[4]="k";
	nenames[5]="z1";
	nenames[6]="z2";
	nenames[7]="bbpsw";
	nenames[8]="tlpsw";
	min.back()=nenames.size();
	max.back()=nenames.size();
	m_dss["ctevents"]=p_file->createDataSet<double>
	  ("ctevents",DataSpace(min,max),props);
	m_necache.reserve(m_ncache);
	for (size_t i(0);i<m_necache.size();++i)
	  m_necache[i].reserve(m_nneprops);
	m_dss["ctevents"].createAttribute<std::string>
	  ("ctevents",DataSpace::From(nenames)).write(nenames);
      }
      min.front()*=nup;
      max.front()*=nup;
      std::vector<std::string> pnames(m_npprops=13);
      pnames[0]="id";
      pnames[1]="status";
      pnames[2]="mother1";
      pnames[3]="mother2";
      pnames[4]="color1";
      pnames[5]="color2";
      pnames[6]="px";
      pnames[7]="py";
      pnames[8]="pz";
      pnames[9]="e";
      pnames[10]="m";
      pnames[11]="lifetime";
      pnames[12]="spin";
      min.back()=pnames.size();
      max.back()=pnames.size();
      props=DataSetCreateProps();
      m_dss["particles"]=p_file->createDataSet<double>
	("particles",DataSpace(min,max),props);
      m_pcache.reserve(m_ncache*nup);
      for (size_t i(0);i<m_pcache.size();++i)
	m_pcache[i].reserve(m_npprops);
      m_dss["particles"].createAttribute<std::string>
	("properties",DataSpace::From(pnames)).write(pnames);
      if (m_hasnlo) {
	std::vector<std::string> npnames(m_nnpprops=4);
	npnames[0]="px";
	npnames[1]="py";
	npnames[2]="pz";
	npnames[3]="e";
	min.back()=npnames.size();
	max.back()=npnames.size();
	m_dss["ctparticles"]=p_file->createDataSet<double>
	  ("ctparticles",DataSpace(min,max),props);
	m_npcache.reserve(m_ncache*nup);
	for (size_t i(0);i<m_npcache.size();++i)
	  m_npcache[i].reserve(m_nnpprops);
	m_dss["ctparticles"].createAttribute<std::string>
	  ("properties",DataSpace::From(npnames)).write(npnames);
      }
      m_nmax=nup;
    }

    void Write(const int &mode=0)
    {
      if (mode!=1 && m_events<m_ncache) return;
      m_events=0;
      auto xfer_props = DataTransferProps{};
      xfer_props.add(UseCollectiveIO{});
      size_t ncache(m_ecache.size());
      std::size_t size(MPI::COMM_WORLD.Get_size());
      std::size_t crank(MPI::COMM_WORLD.Get_rank());
      std::vector<int> ncaches(size,ncache);
      MPI_Allgather(MPI_IN_PLACE,0,MPI_DATATYPE_NULL,&ncaches[0],1,MPI::INT,MPI_COMM_WORLD);
      size_t sumcache(0), rank(0);
      for (size_t i(0);i<size;++i) sumcache+=ncaches[i];
      for (size_t i(0);i<crank;++i) rank+=ncaches[i];
      if (sumcache==0) return;
      for (size_t i(0);i<m_ecache.size();++i) m_ecache[i][2]+=rank*m_nmax;
      rank+=m_offset;
      m_offset+=sumcache;
      m_dss["events"].select({rank,0},{ncache,m_ecache.front().size()}).write(m_ecache,xfer_props);
      m_ecache.clear();
      m_dss["particles"].select({rank*m_nmax,0},{ncache*m_nmax,m_pcache.front().size()}).write(m_pcache,xfer_props);
      m_pcache.clear();
      if (m_hasnlo) {
	m_dss["ctevents"].select({rank,0},{ncache,m_necache.front().size()}).write(m_necache,xfer_props);
	m_necache.clear();
	m_dss["ctparticles"].select({rank*m_nmax,0},{ncache*m_nmax,m_npcache.front().size()}).write(m_npcache,xfer_props);
	m_npcache.clear();
      }
    }

    void Output(const Event &evt)
    {
      ++m_events;
      double wratio(1.0);
      const auto weight(evt.wgts[0]);
      const auto trials(evt.trials);
      size_t nup(evt.size());
      if (nup==0) nup=std::max(evt.pinfo.nplo,evt.pinfo.npnlo+1);
      m_ecache.push_back(std::vector<double>(m_neprops,-1));
      m_ecache.back().push_back(weight*wratio);
      m_ecache.back()[0]=evt.pinfo.pid+1;
      m_ecache.back()[1]=nup;
      m_ecache.back()[3]=m_trials+trials;
      m_trials=0.0;
      m_ecache.back()[4]=evt.muq;
      m_ecache.back()[5]=evt.muf;
      m_ecache.back()[6]=evt.mur;
      m_ecache.back()[8]=evt.aqcd;
      for (int i=0;i<evt.size();++i) {
	Particle p(evt[i]);
	m_pcache.push_back(std::vector<double>(m_npprops,0));
	m_pcache.back()[9]=p.e;
	m_pcache.back()[6]=p.px;
	m_pcache.back()[7]=p.py;
	m_pcache.back()[8]=p.pz;
	m_pcache.back()[10]=p.m;
	m_pcache.back()[0]=p.id;
	m_pcache.back()[12]=p.sp;
	m_pcache.back()[4]=p.cl1;
	m_pcache.back()[5]=p.cl2;
	m_pcache.back()[1]=p.st;
	m_pcache.back()[2]=p.mo1;
	m_pcache.back()[3]=p.mo2;
	m_pcache.back()[11]=0.;
      }
      for (int i(evt.size());i<m_nmax;++i)
	m_pcache.push_back(std::vector<double>(m_npprops,0));
      size_t rank(m_ecache.size()-1);
      m_ecache.back()[2]=(m_offset+rank)*m_nmax;
      if (m_hasnlo) {
	m_necache.push_back(std::vector<double>(m_nneprops,-1));
	if (evt.psw) {
	  m_necache.back()[8]=evt.psw;
	  m_necache.back()[5]=evt.z1;
	  m_necache.back()[6]=evt.z2;
	  m_necache.back()[0]=evt.ijt;
	  m_necache.back()[1]=evt.kt;
	  m_necache.back()[2]=evt.i;
	  m_necache.back()[3]=evt.j;
	  m_necache.back()[4]=evt.k;
	  m_necache.back()[7]=evt.bbpsw;
	}
	for (size_t i(0);i<evt.ctparts.size();++i) {
	  Particle p(evt.ctparts[i]);
	  m_npcache.push_back(std::vector<double>(m_nnpprops,0));
	  m_npcache.back()[0]=p.px;
	  m_npcache.back()[1]=p.py;
	  m_npcache.back()[2]=p.pz;
	  m_npcache.back()[3]=p.e;
	}
	for (int i(evt.ctparts.size());i<m_nmax;++i)
	  m_npcache.push_back(std::vector<double>(m_nnpprops,0));
      }
    }

  };// end of class Output_HDF5

}// end of namespace LHEH5
