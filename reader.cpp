#include "hdf5_event_reader.h"
#define USING__MPI
#ifdef USING__MPI
#include "mpi.h"
#endif

int main(int argc,char **argv)
{
  bool print(false);
  double wmax(0.0);
  long int begin(0), end(std::numeric_limits<long int>::max());
  while (true) {
    switch (getopt(argc,argv,"pm:b:e:h")) {
    case 'p': print=true; continue;
    case 'm': wmax=atof(optarg); continue;
    case 'b': begin=atoi(optarg); continue;
    case 'e': end=atoi(optarg); continue;
    case 'h':
      std::cout<<"usage: "<<argv[0]<<" [options] file.hdf5\n";
      std::cout<<"  -b start   begin reading events at <start>\n";
      std::cout<<"  -e stop    end reading events before <stop>\n";
      std::cout<<"  -m max     print events with relative weight above <max>\n";
      std::cout<<"  -p         print all events\n";
      std::cout<<"  -h         print this help message\n";
      return 0;
    case -1: break;
    }
    break;
  }
  argc-=optind;
  argv+=optind;
  if (argc<1) return -1;
  // create file and read events
  std::string fname=argv[0];
  HighFive::FileAccessProps fprops;
#ifdef USING__MPI
  MPI_Init(&argc,&argv);
  fprops.add(HighFive::MPIOFileAccess(MPI_COMM_WORLD,MPI_INFO_NULL));
  fprops.add(HighFive::MPIOCollectiveMetadata());
#endif
  HighFive::File file(fname,HighFive::File::ReadOnly,fprops);
  LHEH5::LHEFile *e(new LHEH5::LHEFile());
  e->ReadHeader(file);
  double totalxs=e->TotalXS();
  long int nevts=file.getDataSet("events").
    getSpace().getDimensions().front();
  nevts=std::min(nevts,end)-std::max((long int)0,begin);
#ifndef USING__MPI
  int size=1, rank=0;
#else
  int size=MPI::COMM_WORLD.Get_size();
  int rank=MPI::COMM_WORLD.Get_rank();
  MPI_Bcast(&nevts,1,MPI_LONG_INT,0,MPI_COMM_WORLD);
  if (rank==0)
#endif
  std::cout<<"File '"<<fname<<"' -> "<<std::flush;
  size_t iStart=begin+rank*nevts/size;
  size_t iStop=begin+(rank+1)*nevts/size-1;
  if (rank==size-1) iStop=begin+nevts-1;
  e->ReadEvents(file,iStart,iStop-iStart+1);
  // process events (print and calculate cross section)
  double sum[5]={0,0,0,0,0};
  for (size_t i(0);i<e->NEvents();++i) {
    LHEH5::Event evt(e->GetEvent(i));
    if (rank==0 && print) std::cout<<evt<<"\n";
    sum[0]+=evt.trials;
    sum[1]+=evt.wgts[0];
    sum[2]+=evt.wgts[0]*evt.wgts[0];
    sum[3]+=(evt.wgts[0]?1:0);
    sum[4]=std::max(sum[4],std::abs(evt.wgts[0]));
  }
#ifdef USING__MPI
  MPI_Allreduce(MPI_IN_PLACE,sum,4,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE,&sum[4],1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
  if (rank==0)
#endif
  std::cout<<"XS = "<<sum[1]/sum[0]<<" +- "
	   <<sqrt((sum[2]/sum[0]-pow(sum[1]/sum[0],2))/(sum[0]-1))
	   <<" pb, w_max = "<<sum[4]*sum[3]/sum[1]<<"\n";
  if (wmax!=0.0) {
    double max(wmax*e->GetEvent(0).pinfo.xsec*sum[0]/sum[3]);
    for (size_t i(0);i<e->NEvents();++i) {
      LHEH5::Event evt(e->GetEvent(i));
      if (std::abs(evt.wgts[0])>max)
	std::cout<<"Event "<<i<<": "
		 <<evt.wgts[0]*sum[3]/sum[1]<<"\n"
		 <<evt<<"\n";
    }
  }
  delete e;
#ifdef USING__MPI
  MPI_Finalize();
#endif
  return 0;
}
