#include "hdf5_event_writer.h"
#define USING__MPI
#ifdef USING__MPI
#include "mpi.h"
#endif
#include <sys/stat.h>

int main(int argc,char **argv)
{
  if (argc<3) return -1;
  std::string fname=argv[1], oname=argv[2];
  if (fname==oname) return -1;
  struct stat fst;
  if (stat(oname.c_str(),&fst)!=-1) {
    std::cout<<argv[0]<<": file '"<<oname<<"' exists"<<std::endl;
    return -1;
  }
  HighFive::FileAccessProps fprops;
#ifdef USING__MPI
  MPI_Init(&argc,&argv);
  if (MPI::COMM_WORLD.Get_size()>1) {
    std::cout<<argv[0]<<" to be run on single rank only"<<std::endl;
    exit(1);
  }
  fprops.add(HighFive::MPIOFileAccess(MPI_COMM_WORLD,MPI_INFO_NULL));
  fprops.add(HighFive::MPIOCollectiveMetadata());
#endif
  HighFive::File file(fname,HighFive::File::ReadOnly,fprops);
  LHEH5::LHEFile *e(new LHEH5::LHEFile());
  e->ReadHeader(file);
  double totalxs=e->TotalXS();
  long int nevts=file.getDataSet("events").
    getSpace().getDimensions().front();
  std::cout<<"File '"<<fname<<"' -> "<<std::flush;
  size_t iStart=0;
  size_t iStop=nevts-1;
  e->ReadEvents(file,iStart,iStop-iStart+1);
  size_t nup(0), hasnlo(0);
  double nxs(0), nerr(0), unitwgt(0);
  double sum[5]={0,0,0,0,0};
  for (size_t i(0);i<e->NEvents();++i) {
    LHEH5::Event evt(e->GetEvent(i));
    sum[0]+=evt.trials;
    sum[1]+=evt.wgts[0];
    sum[2]+=evt.wgts[0]*evt.wgts[0];
    sum[3]+=(evt.wgts[0]?1:0);
    if (evt.size()) {
      nup=std::max(nup,evt.size());
      nxs=evt.pinfo.xsec;
      nerr=evt.pinfo.err;
      unitwgt=evt.pinfo.unitwgt;
      if (evt.pinfo.npnlo>0) hasnlo=1;
    }
  }
  double max(100);
  if (argc>3) max=atof(argv[3]);
  max*=nxs*sum[0]/sum[3];
  double xs(sum[1]/sum[0]);
  double err(sqrt((sum[2]/sum[0]-pow(sum[1]/sum[0],2))/(sum[0]-1)));
  size_t last(0);
  for (size_t i(0);i<e->NEvents();++i) {
    LHEH5::Event evt(e->GetEvent(i));
    if (std::abs(evt.wgts[0])>max) ++sum[4];
    else if (evt.wgts[0]) last=i;
  }
  std::cout<<"XS = "<<xs<<" +- "<<err<<" pb vs. "<<nxs<<" +- "<<nerr
	   <<" pb -> "<<sum[4]<<" ( "<<sum[3]<<" ) to remove\n";
  LHEH5::LHEOutput *o = new LHEH5::LHEOutput(oname,hasnlo);
  o->Header(e->IData(),e->PInfo());
  o->Initialize(nup,sum[3]-sum[4]);
  double ntrials(0);
  for (size_t i(0);i<e->NEvents();++i) {
    LHEH5::Event evt(e->GetEvent(i));
    if (evt.wgts[0]==0) ntrials+=evt.trials;
    else if (std::abs(evt.wgts[0])>max) {
      std::cout<<"Removing event "<<i<<"\n"<<evt<<"\n";
      continue;
    }
    else {
      evt.trials+=ntrials;
      if (i==last) {
      	for (size_t j(i+1);j<e->NEvents();++j)
      	  evt.trials+=e->GetEvent(j).trials;
      }
      o->Output(evt);
      ntrials=0;
    }
  }
  o->Write(1);
  delete o;
#ifdef USING__MPI
  MPI_Finalize();
#endif
  delete e;
  return 0;
}
