#include "hdf5_event_writer.h"
#define USING__MPI
#ifdef USING__MPI
#include "mpi.h"
#endif
#include <sys/stat.h>

int main(int argc,char **argv)
{
  if (argc<3) return -1;
  std::string fnames[2]={argv[1],argv[2]}, oname=argv[3];
  if (fnames[0]==oname || fnames[1]==oname) return -1;
  struct stat fst;
  if (stat(oname.c_str(),&fst)!=-1) {
    std::cout<<argv[0]<<": file '"<<oname<<"' exists"<<std::endl;
    return -1;
  }
  HighFive::FileAccessProps fprops;
#ifdef USING__MPI
  MPI_Init(&argc,&argv);
  if (MPI::COMM_WORLD.Get_size()>1) {
    std::cout<<argv[0]<<" to be run on single rank only"<<std::endl;
    exit(1);
  }
  fprops.add(HighFive::MPIOFileAccess(MPI_COMM_WORLD,MPI_INFO_NULL));
  fprops.add(HighFive::MPIOCollectiveMetadata());
#endif
  HighFive::File files[2]={
    HighFive::File(fnames[0],HighFive::File::ReadOnly,fprops),
    HighFive::File(fnames[1],HighFive::File::ReadOnly,fprops)};
  LHEH5::LHEFile *es[2]={new LHEH5::LHEFile(),new LHEH5::LHEFile()};
  size_t last[2]={0,0}, nup(0), hasnlo(0);
  double sums[4]={0,0,0,0};
  for (int nf(0);nf<2;++nf) {
    LHEH5::LHEFile *e(es[nf]);
    e->ReadHeader(files[nf]);
    double totalxs=e->TotalXS();
    long int nevts=files[nf].getDataSet("events").
      getSpace().getDimensions().front();
    std::cout<<"File '"<<fnames[nf]<<"' -> "<<std::flush;
    size_t iStart=0;
    size_t iStop=nevts-1;
    e->ReadEvents(files[nf],iStart,iStop-iStart+1);
    double sum[5]={0,0,0,0,0};
    for (size_t i(0);i<e->NEvents();++i) {
      LHEH5::Event evt(e->GetEvent(i));
      sum[0]+=evt.trials;
      sum[1]+=evt.wgts[0];
      sum[2]+=evt.wgts[0]*evt.wgts[0];
      sum[3]+=(evt.wgts[0]?1:0);
      if (evt.size()) {
	nup=std::max(nup,evt.size());
	if (evt.pinfo.npnlo>0) hasnlo=1;
      }
    }
    for (int i(0);i<4;++i) sums[i]+=sum[i];
    double xs(sum[1]/sum[0]);
    double err(sqrt((sum[2]/sum[0]-pow(sum[1]/sum[0],2))/(sum[0]-1)));
    for (size_t i(0);i<e->NEvents();++i)
      if (e->GetEvent(i).wgts[0]) last[nf]=i;
    std::cout<<"XS = "<<xs<<" +- "<<err<<" pb\n";
  }
  if (es[0]->IData()!=es[1]->IData() ||
      es[0]->PInfo()!=es[1]->PInfo()) {
    std::cout<<"Files cannot be merged"<<std::endl;
    delete es[0];
    delete es[1];
    return 2;
  }
  LHEH5::LHEOutput *o = new LHEH5::LHEOutput(oname,hasnlo);
  o->Header(es[0]->IData(),es[0]->PInfo());
  o->Initialize(nup,sums[3]);
  double ntrials(0);
  for (size_t nf(0);nf<2;++nf) {
    LHEH5::LHEFile *e(es[nf]);
    for (size_t i(0);i<e->NEvents();++i) {
      LHEH5::Event evt(e->GetEvent(i));
      if (evt.wgts[0]==0) ntrials+=evt.trials;
      else {
	evt.trials+=ntrials;
	if (i==last[nf]) {
	  for (size_t j(i+1);j<e->NEvents();++j)
	    evt.trials+=e->GetEvent(j).trials;
	}
	o->Output(evt);
	ntrials=0;
      }
    }
  }
  o->Write(1);
  delete o;
#ifdef USING__MPI
  MPI_Finalize();
#endif
  delete es[0];
  delete es[1];
  return 0;
}
